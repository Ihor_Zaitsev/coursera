

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
/**
 * Created by ihorz on 12/21/15.
 */
public class PercolationStats {
    private int T;
    private double[] means;

    public PercolationStats(int N, int T) {
        if (T <= 0 || N <= 0) {
            throw new IllegalArgumentException();
        }
        this.T = T;
        means = new double[T];
        for (int i = 0; i < T; i++) {
            Percolation p = new Percolation(N);
            int count = 0;
            while (!p.percolates()) {
                int x = StdRandom.uniform(N)+1;
                int y = StdRandom.uniform(N)+1;
                if (!p.isOpen(x, y)) {
                    p.open(x, y);
                    count++;
                }
            }
            means[i] = (double) count/(N*N);

        }
    }

    private double sigma() {
        double sum = 0;
        double mu = mu();
        for (double i : means) {
            sum += (i-mu)*(i-mu);
        }

        return Math.sqrt(sum/(T-1));
    }

    private double mu() {
        double sum = 0;
        for (double i : means) {
            sum += i;
        }
        return sum/T;
    }
    public double mean() {
        return StdStats.mean(means);
    }

    public double stddev() {
        return StdStats.stddev(means);
    }

    public double confidenceLo() {
        return (mu()-1.96*sigma()/Math.sqrt(T));
    }

    public double confidenceHi() {
        return (mu()+1.96*sigma()/Math.sqrt(T));
    }

    public static void main(String[] args) {
        PercolationStats ps = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println(ps.mean());
        System.out.println(ps.stddev());
        System.out.println(ps.confidenceLo());
        System.out.println(ps.confidenceHi());
    }
}
