
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * Created by ihorza on 12/15/2015.
 */
public class Percolation {

    private WeightedQuickUnionUF uf;
    private int n;
    private int itemCount;
    private int[] openedArray;

    public Percolation(int N)
    {
        if (N <= 0) {
            throw new IndexOutOfBoundsException();
        } else {
            n = N;
            itemCount = N*N;
            uf = new WeightedQuickUnionUF(itemCount+2);
            openedArray = new int[itemCount+2];
            for (int i = 1; i <= n; i++) {
                uf.union(0, i);
            }
            for (int i = 1; i <= n; i++) {
                uf.union(itemCount+1, convertTo1D(n, i));
            }
        }
    }

    public void open(int i, int j)
    {
        int openedCell = convertTo1D(i, j);
        openedArray[openedCell] = 1;
        int[] nearCells = getNeibhourCells(openedCell);
        for (int ind = 0; ind < nearCells.length; ind++) {
            if (openedArray[nearCells[ind]] == 1) {
                uf.union(nearCells[ind], openedCell);
            }
        }
    }

    private int[] getNeibhourCells(int cellNumber) {
        int[] neibhours = new int[4];
        int nonZero = 0;

        if (cellNumber > n) {
            neibhours[0] = cellNumber -n;
            nonZero++;
        }
        if ((cellNumber-1) % n != 0) {
            neibhours[1] = cellNumber - 1;
            nonZero++;
        }
        if ((cellNumber) % n != 0) {
            neibhours[2] = cellNumber + 1;
            nonZero++;
        }
        if (cellNumber < itemCount - n) {
            neibhours[3] = cellNumber + n;
            nonZero++;
        }

        int[] result = new int[nonZero];
        for (int i = 0, j = 0; i < neibhours.length; i++) {
            if (neibhours[i] != 0) {
                result[j++] = neibhours[i];
            }
        }

        return result;
    }

    public boolean isOpen(int i, int j)
    {
        return openedArray[convertTo1D(i, j)] != 0;
    }

    public boolean isFull(int i, int j)
    {
        return uf.connected(0, convertTo1D(i, j)) && isOpen(i, j);
    }

    public boolean percolates()
    {
        return uf.connected(0, itemCount+1);
    }

    private int convertTo1D(int i, int j) { return ((i-1)*n+j); }

    public static void main(String[] args)
    {
        for (int file = 0; file < args.length; file++) {
            In in = new In(args[file]);
            int N = in.readInt();
            Percolation perc = new Percolation(N);
            while (!in.isEmpty()) {
                int i = in.readInt();
                int j = in.readInt();
                perc.open(i, j);
            }
            System.out.println(perc.percolates());
        }
    }
}
